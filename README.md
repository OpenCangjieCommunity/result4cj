<div align="center">
<h1>result4cj</h1>
</div>

<p align="center">
<img alt="" src="https://img.shields.io/badge/release-v1.0.0-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/build-pass-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjc-v0.50.3-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/project-open-brightgreen" style="display: inline-block;" />
</p>

## <img alt="" src="./doc/assets/readme-icon-introduction.png" style="display: inline-block;" width=3%/> 简介

- Result异常处理模型。

### API文档

[Result 文档](./doc/api/result/result.md)

### 源码目录

```shell
.
├── README.md
├── doc 
├── src
│   └── result
|      |── extend.cj
|      |── result_exception.cj
|      └── result.cj
└── test   
    ├── HLT
    ├── LLT
    └── UT
```

- `doc` 存放库的设计文档、使用文档、LLT 用例覆盖报告(1.由于仓颉的编码自动转成 UTF8 ，所以有很多与编码格式相关的都覆盖不到；2.做冗余校验无法实际覆盖到)
- `src` 是库源码目录
- `test` 存放 HLT 测试用例、LLT 自测用例和 UT 单元测试用例



## <img alt="" src="./doc/assets/readme-icon-compile.png" style="display: inline-block;" width=3%/> 使用说明

### 引用仓库

```json
{
  "result4cj": {
    "branch": "master",
    "version": "1.0.0",
    "git": "https://gitee.com/HW-PLLab/result4cj"
  }
}
```

### 编译

```
cjpm build -V
```




## <img alt="" src="./doc/assets/readme-icon-contribute.png" style="display: inline-block;" width=3%/> 参与贡献

欢迎给我们提交 PR，欢迎给我们提交 issue，欢迎参与任何形式的贡献。

[穗鸿仓](https://gitee.com/suihongcang)