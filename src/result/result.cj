package result4cj.result

const Generics_E_Is_Not_Impl_ToString_Interface = "<E> is not impl ToString interface"

public enum Result<T, E> {
    | Ok(T)
    | Err(E)

    public func isOk(): Bool {
        match (this) {
            case Ok(v) => true

            case _ => false
        }
    }

    public func isOkAnd(f: (T) -> Bool): Bool {
        match (this) {
            case Ok(v) => f(v)

            case _ => false
        }
    }

    public func isErr(): Bool {
        match (this) {
            case Err(_) => true

            case _ => false
        }
    }

    public func isErrAnd(f: (E) -> Bool): Bool {
        match (this) {
            case Err(e) => f(e)

            case _ => false
        }
    }

    public func ok(): ?T {
        match (this) {
            case Ok(v) => v

            case _ => None
        }
    }

    public func err(): ?E {
        match (this) {
            case Err(e) => e

            case _ => None
        }
    }

    public func map<U>(op: (T) -> U): Result<U, E> {
        match (this) {
            case Ok(v) => Ok(op(v))

            case Err(e) => Err(e)
        }
    }

    public func mapOr<U>(defaultValue: U, op: (T) -> U): U {
        match (this) {
            case Ok(v) => op(v)

            case _ => defaultValue
        }
    }

    public func mapOrElse<U>(defaultValueOp: (E) -> U, op: (T) -> U): U {
        match (this) {
            case Ok(v) => op(v)

            case Err(e) => defaultValueOp(e)
        }
    }

    public func mapErr<F>(op: (E) -> F): Result<T, F> {
        match (this) {
            case Ok(v) => Ok(v)

            case Err(e) => Err(op(e))
        }
    }

    public func inspect(f: (T) -> Unit): Result<T, E> {
        match (this) {
            case Ok(v) => f(v)

            case _ => ()
        }
        this
    }

    public func inspectErr(f: (E) -> Unit): Result<T, E> {
        match (this) {
            case Err(e) => f(e)

            case _ => ()
        }
        this
    }

    public func expect(msg: String): T {
        match (this) {
            case Ok(v) => v

            case Err(e) =>
                let err = mapErrMsg<E>(e)
                throw ResultException("${msg}: ${err}")
        }
    }

    public func unwrap(): T {
        match (this) {
            case Ok(v) => v

            case Err(e) =>
                let err = mapErrMsg<E>(e)
                throw ResultException(err)
        }
    }

    public func expectErr(msg: String): E {
        match (this) {
            case Ok(v) =>
                let err = (v as ToString)?.toString() ?? ""
                throw ResultException("${msg}: ${err}")

            case Err(e) => e
        }
    }

    public func unwrapErr(): E {
        match (this) {
            case Ok(v) =>
                let err = (v as ToString)?.toString() ?? ""
                throw ResultException(err)

            case Err(e) => e
        }
    }

    public func and<U>(res: Result<U, E>): Result<U, E> {
        match (this) {
            case Ok(_) => res

            case Err(e) => Err(e)
        }
    }

    public func andThen<U>(op: (T) -> Result<U, E>): Result<U, E> {
        match (this) {
            case Ok(v) => op(v)

            case Err(e) => Err(e)
        }
    }

    public func or<F>(res: Result<T, F>): Result<T, F> {
        match (this) {
            case Ok(v) => Ok(v)

            case Err(e) => res
        }
    }

    public func orElse<F>(op: (E) -> Result<T, F>): Result<T, F> {
        match (this) {
            case Ok(v) => Ok(v)

            case Err(e) => op(e)
        }
    }

    public func unwrapOr(defaultValue: T): T {
        match (this) {
            case Ok(v) => v

            case Err(e) => defaultValue
        }
    }

    public func unwrapOrElse(op: (E) -> T): T {
        match (this) {
            case Ok(v) => v

            case Err(e) => op(e)
        }
    }

    public func copied(): Result<T, E> {
        match (this) {
            case Ok(v) => Ok(v)

            case Err(e) => Err(e)
        }
    }

    public static func transpose<T>(v: Result<Option<T>, E>): Option<Result<T, E>> {
        match (v) {
            case Ok(o) => if (let Some(ov) <- o) {
                Ok(ov)
            } else {
                None
            }

            case Err(e) => Err(e)
        }
    }

    public static func flatten<T>(v: Result<Result<T, E>, E>): Result<T, E> {
        match (v) {
            case Ok(o) => match (o) {
                case Ok(ov) => Ok(ov)

                case Err(e) => Err(e)
            }

            case Err(e) => Err(e)
        }
    }
}

func mapErrMsg<E>(e: E): String {
    (e as ToString)?.toString() ?? Generics_E_Is_Not_Impl_ToString_Interface
}
