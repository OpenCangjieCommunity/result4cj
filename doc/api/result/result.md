## 类型的错误处理

Rust的`Result`类型的错误处理，具体参照[enum.Result](https://rustwiki.org/zh-CN/std/result/enum.Result.html)。

`Result<T, E>`是用于返回和传播错误的类型。 它是一个带有变体的枚举，`Ok(T)`，表示成功并包含一个值，而`Err(E)`表示错误并包含一个错误值

### `Result` 枚举类

#### 函数对照表

| Rust函数 | 仓颉函数 | 备注 |
| :--- | :--- | :---: |
| `pub const fn is_ok(&self) -> bool` | `public func isOk(): Bool` |
| `pub fn is_ok_and(self, f: impl FnOnce(T) -> bool) -> bool` | `public func isOkAnd(f: (T) -> Bool): Bool` |
| `pub const fn is_err(&self) -> bool` | `public func isErr(): Bool` |
| `pub fn is_err_and(self, f: impl FnOnce(E) -> bool) -> bool` | `public func isErrAnd(f: (E) -> Bool): Bool` |
| `pub fn ok(self) -> Option<T>` | `public func ok(): ?T` |
| `pub fn err(self) -> Option<E>` | `public func err(): ?E` |
| `pub fn map<U, F>(self, op: F) -> Result<U, E>` | `public func map<U>(op: (T) -> U): Result<U, E>` |
| `pub fn map_or<U, F>(self, default: U, f: F) -> U` | `public func mapOr<U>(defaultValue: U, op: (T) -> U): U` |
| `pub fn map_or_else<U, D, F>(self, default: D, f: F) -> U` | `public func mapOrElse<U>(defaultValueOp: (E) -> U, op: (T) -> U): U` |
| `pub fn map_err<F, O>(self, op: O) -> Result<T, F>` | `public func mapErr<F>(op: (E) -> F): Result<T, F>` |
| `pub fn inspect<F>(self, f: F) -> Result<T, E>` | `public func inspect(f: (T) -> Unit): Result<T, E>` |
| `pub fn inspect_err<F>(self, f: F) -> Result<T, E>` | `public func inspectErr(f: (E) -> Unit): Result<T, E>` |
| `pub fn expect(self, msg: &str) -> T` | `public func expect(msg: String): T` |
| `pub fn unwrap(self) -> T` | `public func unwrap(): T` |
| `pub fn unwrap_or_default(self) -> T` | `public func unwrapOrDefault(): T` | `where T <: Default<T>` |
| `pub fn expect_err(self, msg: &str) -> E` | `public func expectErr(msg: String): E` |
| `pub fn unwrap_err(self) -> E` | `public func unwrapErr(): E` |
| `pub fn and<U>(self, res: Result<U, E>) -> Result<U, E>` | `public func and<U>(res: Result<U, E>): Result<U, E>` |
| `pub fn and_then<U, F>(self, op: F) -> Result<U, E>` | `public func andThen<U>(op: (T) -> Result<U, E>): Result<U, E>` |
| `pub fn or<F>(self, res: Result<T, F>) -> Result<T, F>` | `public func or<F>(res: Result<T, F>): Result<T, F>` |
| `pub fn or_else<F, O>(self, op: O) -> Result<T, F>` | `public func orElse<F>(op: (E) -> Result<T, F>): Result<T, F>` |
| `pub fn unwrap_or(self, default: T) -> T` | `public func unwrapOr(defaultValue: T): T` |
| `pub fn unwrap_or_else<F>(self, op: F) -> T` | `public func unwrapOrElse(op: (E) -> T): T` |
| `pub fn copied(self) -> Result<T, E>` | `public func copied(): Result<T, E>` |
| `pub fn cloned(self) -> Result<T, E>` | `public func cloned(): Result<T, E>` | `where T <: Cloneable<T>` |
| `pub fn transpose(self) -> Option<Result<T, E>>` | `public static func transpose<T>(v: Result<Option<T>, E>): Option<Result<T, E>>` |
| `pub fn flatten(self) -> Result<T, E>` | `public static func flatten<T>(v: Result<Result<T, E>, E>): Result<T, E>` |


#### `Default<T>` 接口

```cangjie
public interface Default<T> {
    public static func get(): T
}
```


#### `Cloneable<T>` 接口

```cangjie
public interface Default<T> {
    public func clone(): T
}
```


### 示例

```cangjie
from result4cj import result.*

main() {
    let i: Result<Int64, Exception> = Ok(123)
    println("i = ${i.unwrap()})
}
```
